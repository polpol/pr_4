import math
import unittest


class InputCheckingTestCase(unittest.TestCase):
    def test_positive_radiuses(self):
        """
        Проверим, когда input_checking(radius) возвращает список чисел.

        Проверка для списка положительных целых чисел input_checking([2, 4, 5].
        """
        self.assertEqual(input_checking([2, 4, 5]), [2, 4, 5])
    def test_invalid_radiuses(self):
        """
        Проверим случаи вывода ValueError для input_checking(radius).

        Проверка, если radius[i] не является положительным целым числом или radius
        не задан. 
        """
        self.assertRaises(ValueError, input_checking, [])
        self.assertRaises(ValueError, input_checking, [0, 3])
        self.assertRaises(ValueError, input_checking, [5, 7, 'Two'])
    def test_areas(self):
        """
        Проверим, что areas(radius) возвращает верный результат вычисления.

        Проверка для areas([2, 4, 5]) и areas([1, 3, 6]).
        """
        self.assertEqual(areas([2, 4, 5]), (40.8407, 37.6991, 0.52, 0.48))
        self.assertEqual(areas([1, 3, 6]), (87.9646, 25.1327, 0.7778, 0.2222)) 

def input_checking(radius):
    """
    Проверка, являются ли элементы списка целыми положительными числами. 

    Принимает на вход список чисел.
    Возвращает введенное значение или выводит исключение ValueError.

    Args:
        radius:  Список радиусов. 

    Returns:
        Список положительных чисел.

    Raises:
        ValueError


    Examples:
        >>> input_checking([])
        Traceback (most recent call last):
        	...
        ValueError: Значения не заданы
        
        >>> myfunction([5, 0, 'One'])
        Traceback (most recent call last):
      		...
      	ValueError: Радиусы должны являться положительными числами
      	
      	>>> input_checking([2, 5, 4])
        [2, 5, 4]
    """
    if len(radius) > 0:
        if str(radius[len(radius)-1]).isdigit() and int(radius[0]) > 0:
            for i in range(len(radius)):
                radius[i] = int(radius[i])
            return radius
        else:
            raise ValueError('Радиусы должны являться положительными числами')
    else:
        raise ValueError('Значения не заданы')

def areas(radius):
    """
    Вычисление сумм площадей для соседних областей окружностей (радиус
    
    окружностей определяется принимаемыми значениями) и вероятностй попадения
    в эти области.    
    Принимает на вход список положительных целых чисел.
    Возвращает четыре числа, округленных до четырех значащий цифр.

    Args:
        radius:  Список радиусов. 

    Returns:
        Результат вычислений, преобразованный в кортеж.    


    Examples:
        >>> areas([2, 4, 5])
        (40.8407, 37.6991, 0.52, 0.48)
    """
    radius = list(set(radius))
    total_blue_area = 0
    for i in range(1, len(radius), 2):
        blue_area = (radius[i] * radius[i] * math.pi - radius[i - 1] *
                     radius[i - 1] * math.pi)
        total_blue_area += blue_area
    red_area = (radius[len(radius) - 1] * radius[len(radius) - 1] *
                math.pi - total_blue_area)
    chance_blue = total_blue_area / (total_blue_area + red_area)
    chance_red = 1 - chance_blue
    return (round(red_area, 4), round(total_blue_area, 4),
            round(chance_red, 4), round(chance_blue, 4))

def main():
    """
    Содержит ввод данных с клавиатуры, вызов функции и вывод результата.
    """
    radius = input('Введите радиусы: ').split()
    radius.sort()
    input_checking(radius)
    print('Площадь красного цвета {0}, синего {1}.\
          Вероятность попадения в красную область {2}, в синюю {3}.'.
          format(areas(radius)[0], areas(radius)[1],\
                 areas(radius)[2], areas(radius)[3]))


if __name__ == "__main__":
    unittest.main()    
    
